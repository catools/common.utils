package org.catools.common.utils;

import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.defaultIfBlank;

@UtilityClass
public class CConfigUtil {
    public static Map<String, String> readArgs(String[] args) {
        Map<String, String> params = new HashMap<>();
        for (String arg : args) {
            if (arg.contains("=")) {
                int idx = arg.indexOf("=");
                System.setProperty(arg.substring(2, idx).trim(), arg.substring(idx + 1).trim());
                params.put(arg.substring(2, idx).trim(), arg.substring(idx + 1).trim());
            } else {
                throw new RuntimeException(String.format("Invalid Argument %s Arguments should be in maven format (i.e. -Dkey=value)", arg));
            }
        }
        return params;
    }

    public static String getValueOrDefault(String configName, String defaultValue) {
        return defaultIfBlank(defaultIfBlank(System.getProperty(configName), System.getenv(configName)), defaultValue);
    }

    public static String getRunName() {
        return StringUtils.defaultString(System.getProperty("RUN_NAME"));
    }

    public static void setRunName(String value) {
        setProperty("RUN_NAME", value);
    }

    public static <T extends Enum<T>> void setProperty(T config, String value) {
        setProperty(config.name(), value);
    }

    public static void setProperty(String config, String value) {
        System.setProperty(config, CStringUtil.defaultString(value));
    }

    public static boolean getBoolean(String config) {
        String prop = getString(config);
        return !StringUtils.EMPTY.equals(prop) && Boolean.valueOf(prop).booleanValue();
    }

    public static <T extends Enum<T>> boolean getBoolean(T config) {
        return getBoolean(config.name());
    }

    public static boolean getBooleanOrElse(String config, boolean defaultIfNotSet) {
        return has(config) ? getBoolean(config) : defaultIfNotSet;
    }

    public static <T extends Enum<T>> boolean getBooleanOrElse(T config, boolean defaultIfNotSet) {
        return getBooleanOrElse(config.name(), defaultIfNotSet);
    }

    public static List<Boolean> getBooleans(String config, String delimiter) {
        return getStrings(config, delimiter).stream().map(Boolean::valueOf).collect(Collectors.toList());
    }

    public static <T extends Enum<T>> List<Boolean> getBooleans(T config, String delimiter) {
        return getBooleans(config.name(), delimiter);
    }

    public static List<Boolean> getBooleansOrElse(String config, String delimiter, List<Boolean> defaultIfNotSet) {
        return has(config) ? getBooleans(config, delimiter) : defaultIfNotSet;
    }

    public static <T extends Enum<T>> List<Boolean> getBooleansOrElse(T config, String delimiter, List<Boolean> defaultIfNotSet) {
        return getBooleansOrElse(config.name(), delimiter, defaultIfNotSet);
    }

    public static <T extends Enum<T>> double getDouble(T config) {
        return getDouble(config.name());
    }

    public static double getDouble(String config) {
        String prop = getString(config);
        return StringUtils.EMPTY.equals(prop) ? 0 : Double.valueOf(prop).doubleValue();
    }

    public static <T extends Enum<T>> double getDoubleOrElse(T config, Double defaultIfNotSet) {
        return getDoubleOrElse(config.name(), defaultIfNotSet);
    }

    public static double getDoubleOrElse(String config, Double defaultIfNotSet) {
        return has(config) ? getDouble(config) : defaultIfNotSet;
    }

    public static <T extends Enum<T>> int getInteger(T config) {
        return getInteger(config.name());
    }

    public static int getInteger(String config) {
        String prop = getString(config);
        return StringUtils.EMPTY.equals(prop) ? 0 : Integer.valueOf(prop).intValue();
    }

    public static <T extends Enum<T>> int getIntegerOrElse(T config, Integer defaultIfNotSet) {
        return getIntegerOrElse(config.name(), defaultIfNotSet);
    }

    public static int getIntegerOrElse(String config, Integer defaultIfNotSet) {
        return has(config) ? getInteger(config) : defaultIfNotSet;
    }

    public static <T extends Enum<T>> List<Integer> getIntegers(T config, String delimiter) {
        return getIntegers(config.name(), delimiter);
    }

    public static List<Integer> getIntegers(String config, String delimiter) {
        return getStrings(config, delimiter).stream().map(Integer::valueOf).collect(Collectors.toList());
    }

    public static <T extends Enum<T>> List<Integer> getIntegersOrElse(T config, String delimiter, List<Integer> defaultIfNotSet) {
        return getIntegersOrElse(config.name(), delimiter, defaultIfNotSet);
    }

    public static List<Integer> getIntegersOrElse(String config, String delimiter, List<Integer> defaultIfNotSet) {
        return has(config) ? getIntegers(config, delimiter) : defaultIfNotSet;
    }

    public static <T extends Enum<T>> String getString(T config) {
        return getString(config.name());
    }

    public static String getString(String config) {
        return getValueOrDefault(config, "");
    }

    public static <T extends Enum<T>> String getStringOrElse(T config, String defaultIfNotSet) {
        return getStringOrElse(config.name(), defaultIfNotSet);
    }

    public static String getStringOrElse(String config, String defaultIfNotSet) {
        return has(config) ? getString(config) : defaultIfNotSet;
    }

    public static <T extends Enum<T>> List<String> getStrings(T config, String delimiter) {
        return getStrings(config.name(), delimiter);
    }

    public static List<String> getStrings(String config, String delimiter) {
        String prop = getString(config);
        return StringUtils.isBlank(prop) ? new ArrayList<>() : Arrays.asList(prop.split(delimiter));
    }

    public static <T extends Enum<T>> List<String> getStringsOrElse(T config, String delimiter, List<String> defaultIfNotSet) {
        return getStringsOrElse(config.name(), delimiter, defaultIfNotSet);
    }

    public static List<String> getStringsOrElse(String config, String delimiter, List<String> defaultIfNotSet) {
        return has(config) ? getStrings(config, delimiter) : defaultIfNotSet;
    }

    public static <T extends Enum<T>> boolean has(T config) {
        return has(config.name());
    }

    public static boolean has(String config) {
        return getValueOrDefault(config, null) != null;
    }
}
