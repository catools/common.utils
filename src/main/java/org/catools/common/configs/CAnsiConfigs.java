package org.catools.common.configs;

import org.catools.common.utils.CAnsiUtil;
import org.catools.common.utils.CConfigUtil;

public class CAnsiConfigs {
    public static boolean isPrintInAnsiEnable() {
        return CConfigUtil.getBooleanOrElse(Configs.PRINT_IN_ANSI_ENABLE, true);
    }

    public static boolean isPrintInColorAvailable() {
        return CAnsiUtil.isOutputSupportAnsi() && isPrintInAnsiEnable();
    }

    private enum Configs {
        PRINT_IN_ANSI_ENABLE
    }
}
